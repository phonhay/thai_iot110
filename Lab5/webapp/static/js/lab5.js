$(document).ready(function() {
    var led1;
    var led2;
    var led3;

    // Dictionaries to store cached data.
    var cached_sensor_readings = [];
    var cached_temp_graph_data = [];
    var cached_press_graph_data = [];

    // MorrisJS graph structures
    var tempGraph = new Morris.Line({

	element: 'mytempchart',

	data: [],

	xkey: 'time',
	ykeys: ['temp'],
	labels: ['Temperature (C)']
,
	hideHover: 'auto',
	ymax: 'auto',
	ymin: 'auto'
    });


    var pressGraph = new Morris.Line({

	element: 'mypresschart',

	data: [],

	xkey: 'time',
	ykeys: ['press'],
	labels: ['Pressure (hPa)']
,
	hideHover: 'auto',
	ymax: 'auto',
	ymin: 'auto'
    });


    // Message handler.
    iotSource.onmessage = function(e) {
 	sse_data = JSON.parse(e.data);
	console.log(sse_data);

	updateSwitch(sse_data["switch"]);
	updateLeds(1, sse_data["led_red"]);
	updateLeds(2, sse_data["led_grn"]);
	updateLeds(3, sse_data["led_blu"]);

	cached_sensor_readings.push(sse_data);
	while (cached_sensor_readings.length > 5) cached_sensor_readings.shift();

	updateDataTable();
	updateTemperatureChart();
	updatePressureChart();
    }

    // Update the data table.
    function updateDataTable() {
	rStr = "";
	cached_sensor_readings.forEach(function(di) {
	    rStr += "<tr>";
	    rStr += "<td>" + di['meas_time'] + "</td>";
 	    rStr += "<td>" + di['temperature']['reading'].toFixed(2) + "</td>";
	    rStr += "<td>" + di['pressure']['reading'].toFixed(4) + "</td>";
	    rStr += "</tr>";
	});

	$("tbody#sensor-data").html(rStr);
    }

    // Update the temperature chart.
    function updateTemperatureChart() {
	cached_temp_graph_data.push({'time': Date.parse(cached_sensor_readings[0]['meas_time']), 'temp': cached_sensor_readings[0]['temperature']['reading']});
	while (cached_temp_graph_data.length > 20) cached_temp_graph_data.shift();

  	tempGraph.setData(cached_temp_graph_data);

    }

    // Update the pressure chart.
    function updatePressureChart() {
	cached_press_graph_data.push({'time': Date.parse(cached_sensor_readings[0]['meas_time']), 'press': cached_sensor_readings[0]['pressure']['reading']});
	while (cached_press_graph_data.length > 20) cached_press_graph_data.shift();

  	pressGraph.setData(cached_press_graph_data);

    }

    // Update the Switch based on its SSE state monitor
    function updateSwitch(switchValue) {
        if (switchValue === '1') {
            // $('#switch').text(9759);
            $('#switch').toggleClass('label-default', false);
            $('#switch').toggleClass('label-success', true);
        } else if (switchValue === '0') {
            // $('#switch').text('OFF');
            $('#switch').toggleClass('label-default', true);
            $('#switch').toggleClass('label-success', false);
        }
    }

    // Update the LEDs based on their SSE state monitor
    function updateLeds(ledNum, ledValue) {
        if (ledNum === 1) {
            if (ledValue === '1') {
                $('#red_led_label').toggleClass('label-default', false);
                $('#red_led_label').toggleClass('label-danger', true);
                led1 = "ON"
            } else if (ledValue === '0') {
                $('#red_led_label').toggleClass('label-default', true);
                $('#red_led_label').toggleClass('label-danger', false);
                led1 = "OFF"
            }
        } else if (ledNum === 2) {
            if (ledValue === '1') {
                $('#grn_led_label').toggleClass('label-default', false);
                $('#grn_led_label').toggleClass('label-success', true);
                led2 = "ON"
            } else if (ledValue === '0') {
                $('#grn_led_label').toggleClass('label-default', true);
                $('#grn_led_label').toggleClass('label-success', false);
                led2 = "OFF"
            }
        } else if (ledNum === 3) {
            if (ledValue === '1') {
                $('#blu_led_label').toggleClass('label-default', false);
                $('#blu_led_label').toggleClass('label-primary', true);
                led3 = "ON"
            } else if (ledValue === '0') {
                $('#blu_led_label').toggleClass('label-default', true);
                $('#blu_led_label').toggleClass('label-primary', false);
                led3 = "OFF"
            }
        }
    }

    // The button click functions run asynchronously in the browser
    $('#red_led_btn').click(function() {
        if (led1 === "OFF") { led1 = "ON"; } else { led1 = "OFF"; }
        var params = 'led=1&state=' + led1;
        console.log('Led Command with params:' + params);
        $.post('/ledcmd', params, function(data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
        });
    });

    // The button click functions run asynchronously in the browser
    $('#grn_led_btn').click(function() {
        if (led2 === "OFF") { led2 = "ON"; } else { led2 = "OFF"; }
        var params = 'led=2&state=' + led2;
        console.log('Led Command with params:' + params);
        $.post('/ledcmd', params, function(data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
        });
    });

    // The button click functions run asynchronously in the browser
    $('#blu_led_btn').click(function() {
        if (led3 === "OFF") { led3 = "ON"; } else { led3 = "OFF"; }
        var params = 'led=3&state=' + led3;
        console.log('Led Command with params:' + params);
        $.post('/ledcmd', params, function(data, status) {
            console.log("Data: " + data + "\nStatus: " + status);
        });
    });
});